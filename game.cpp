
#include "stdafx.h"
#include <map>
#include "game.h"

#define MAX_LOADSTRING 100

#pragma region GLOBAL_VARIABLES
const int size = 200;
const int windowSize = 600;
int WhatIAmGoingToDraw=0;
bool fieldsCreated=false;
bool GameInProgress = false;
bool EndOfGame = false;
char wyniki [3][3];
HWND hWndOfMainWindow;
HWND handlers[9]; 
HBRUSH brushPola = CreateSolidBrush(RGB(30, 30, 30));

struct BoolAndCharOfWindow
{
	char znak;
	bool czynarysowano;
};
BoolAndCharOfWindow StworzBoolAndCharOfWindow(char znak, bool czynarysowano)
{
	BoolAndCharOfWindow x = BoolAndCharOfWindow();
	x.czynarysowano=czynarysowano;
	x.znak=znak;
	return x;
}

std::map<HWND,BoolAndCharOfWindow> mapa;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
#pragma endregion

ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

#pragma region FUNCTIONS
void SetWindowBackColor(HDC WindowDC, COLORREF BackColor)
{
	HWND HandleWindow= WindowFromDC(WindowDC);
	RECT WindowClientRectangle;
	GetClientRect(HandleWindow,&WindowClientRectangle);
	SelectObject(WindowDC, GetStockObject(DC_BRUSH)); //selecting the brush
	SetDCBrushColor(WindowDC,BackColor);
	SetDCPenColor(WindowDC,BackColor);
	Rectangle(WindowDC,WindowClientRectangle.left, WindowClientRectangle.top,WindowClientRectangle.right,WindowClientRectangle.bottom);
}

void SprawdzCzyJuzEndOfGame()
{	
	for(int i=0;i<3;i++)
	{
		if((wyniki[i][0]=='X' && wyniki[i][1]=='X' && wyniki[i][2]=='X') || (wyniki[0][i]=='X' && wyniki[1][i]=='X' && wyniki[2][i]=='X') || (wyniki[0][0]=='X' && wyniki[1][1]=='X' && wyniki[2][2]=='X') || (wyniki[0][2]=='X' && wyniki[1][1]=='X' && wyniki[2][0]=='X'))
		{
			EndOfGame=true;
			SetWindowText(hWndOfMainWindow,L"Koniec gry.");
			MessageBox(hWndOfMainWindow,L"Krzy�yk wygra�.",L"Koniec gry",MB_OK);
			WhatIAmGoingToDraw=0;
			return;
		}
		if((wyniki[i][0]=='O' && wyniki[i][1]=='O' && wyniki[i][2]=='O') || (wyniki[0][i]=='O' && wyniki[1][i]=='O' && wyniki[2][i]=='O') || (wyniki[0][0]=='O' && wyniki[1][1]=='O' && wyniki[2][2]=='O') || (wyniki[0][2]=='O' && wyniki[1][1]=='O' && wyniki[2][0]=='O'))
		{
			EndOfGame=true;
			SetWindowText(hWndOfMainWindow,L"Koniec gry.");
			MessageBox(hWndOfMainWindow,L"K�ko wygra�o.",L"Koniec gry",MB_OK);
			WhatIAmGoingToDraw=1;
			return;
		}

	}
	bool zmienna=true;
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			if(wyniki[i][j]=='n')
			{
				zmienna=false;
				goto label;
			}
		}
	}
	label:
	if(EndOfGame==true && GameInProgress==true)
		return;
	EndOfGame=zmienna;
	if(zmienna==true && GameInProgress==true)
	{
		SetWindowText(hWndOfMainWindow,L"Koniec gry.");
	}
}

void RozpocznijGre(HWND hWnd)
{
	GameInProgress=true;
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			wyniki[i][j]='n';
		}
	}
	EndOfGame = false;
	EnableMenuItem(GetMenu(hWnd), ID_MENU_ZAKO32772, MF_ENABLED);
	EnableMenuItem(GetMenu(hWnd), ID_MENU_ROZPOCZNIJGR32771, MF_GRAYED);

	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			handlers[i*3 + j] = CreateWindow(szWindowClass,szTitle,WS_CHILD | WS_BORDER | WS_VISIBLE, i * 200, j * 200, 200, 200,hWnd, NULL, hInst, NULL);
			SetMenu(handlers[i*3 + j], NULL);

		}
	}
	mapa = std::map<HWND,BoolAndCharOfWindow>();
	std::map<HWND,BoolAndCharOfWindow>::iterator it = mapa.begin();
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			mapa.insert(it, std::pair<HWND,BoolAndCharOfWindow>(handlers[i*3+j],StworzBoolAndCharOfWindow('\0',false))); 

		}
	}
	fieldsCreated = true;
	if(WhatIAmGoingToDraw==0)
	{
		SetWindowText(hWnd,L"K�ko rozpoczyna gr�.");
	}
	else
	{
		SetWindowText(hWnd,L"Krzy�yk rozpoczyna gr�.");
	}
}

void ZakonczGre(HWND hWnd)
{
	if(fieldsCreated)
	{
		for(int i = 0; i < 9; i++)
		{
			DestroyWindow(handlers[i]);
		}
	}
	GameInProgress=false;
	fieldsCreated = false;
	EnableMenuItem(GetMenu(hWnd), ID_MENU_ROZPOCZNIJGR32771, MF_ENABLED);
	EnableMenuItem(GetMenu(hWnd), ID_MENU_ZAKO32772, MF_GRAYED);
	SetWindowText(hWnd,L"K�ko i krzy�yk.");
}

void LeftButtonCallback(HWND hWnd)
{
	HDC hdc;
	auto it = mapa.find(hWnd);

	if(fieldsCreated==true && it->second.czynarysowano == false)
	{
		if(WhatIAmGoingToDraw==0)
		{
			hdc = GetDC(hWnd);
			SetWindowBackColor(hdc,RGB(255,0,0));
			HPEN hEllipsePen;
			COLORREF qEllipseColor;
			qEllipseColor = RGB(0, 0, 255);
			hEllipsePen = CreatePen(PS_SOLID, 8, qEllipseColor);
			HPEN hPenOld = (HPEN)SelectObject(hdc, hEllipsePen);
			Arc(hdc, 75, 75, 125, 125, 0, 0, 0, 0);

			SelectObject(hdc, hPenOld);
			DeleteObject(hEllipsePen);

			for(int i=0;i<3;i++)
			{
				for(int j=0;j<3;j++)
				{
					if(hWnd==handlers[i*3+j])
					{
						wyniki[i][j]='O';
						goto dalej;
					}
				}
			}
dalej:
			WhatIAmGoingToDraw=1;
			it->second.czynarysowano = true;
			it->second.znak='X';
			SetWindowText(hWndOfMainWindow,L"Krzy�yk wykonaj sw�j ruch.");
		}
		else if(WhatIAmGoingToDraw==1)
		{
			hdc = GetDC(hWnd);
			SetWindowBackColor(hdc,RGB(0,0,255));
			WhatIAmGoingToDraw=0;
			HPEN hLinePen;
			COLORREF qLineColor;
			qLineColor = RGB(255, 0, 0);
			hLinePen = CreatePen(PS_SOLID, 8, qLineColor);
			HPEN hPenOld = (HPEN)SelectObject(hdc, hLinePen);

			MoveToEx(hdc, 75, 75, NULL);
			LineTo(hdc, 125, 125);
			MoveToEx(hdc, 125, 75, NULL);
			LineTo(hdc, 75, 125);

			SelectObject(hdc, hPenOld);
			DeleteObject(hLinePen);
			for(int i=0;i<3;i++)
			{
				for(int j=0;j<3;j++)
				{
					if(hWnd==handlers[i*3+j])
					{
						wyniki[i][j]='X';
						goto dalej2;
					}
				}
			}
dalej2:
			SelectObject(hdc, hPenOld);
			it->second.czynarysowano = true;
			it->second.znak='O';
			SetWindowText(hWndOfMainWindow,L"K�ko wykonaj sw�j ruch.");
		}
	}
}
#pragma endregion

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPTSTR lpCmdLine, _In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_game, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_game));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_game));
	wcex.hCursor		= LoadCursor(NULL, IDC_CROSS);
	wcex.hbrBackground	= (HBRUSH)(CreateSolidBrush(RGB(0, 0, 0)));
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_game);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable
	int x = GetSystemMetrics(SM_CXMAXIMIZED);
	int y = GetSystemMetrics(SM_CYMAXIMIZED);
	hWnd = CreateWindow(szWindowClass, L"K�ko i krzy�yk", WS_SYSMENU, x/2 - 315, y/2 - 315, 615, 655, NULL, NULL, hInstance, NULL);
	hWndOfMainWindow = hWnd;

	if (!hWnd)
	{
		return FALSE;
	}
	EnableMenuItem(GetMenu(hWnd), ID_MENU_ZAKO32772, MF_GRAYED);
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	RECT* changed=(RECT*)lParam;
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			if(EndOfGame==true)
			{
				return 0;
			}
			LeftButtonCallback(hWnd);
			SprawdzCzyJuzEndOfGame();
		}		
		break;  
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		RECT rc;
		GetWindowRect(hWnd, &rc);
		switch (wmId)
		{
		case ID_POMOC_ZASADYGRY:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_MENU_WYJ32773:
			{
				INT_PTR a=DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, About);
				if(a==IDCANCEL)
				{
					break;
				}
				else if(a==IDOK)
				{
					DestroyWindow(hWnd);
				}
			}
			break;
		case ID_MENU_ROZPOCZNIJGR32771:
			RozpocznijGre(hWnd);
			break;
		case ID_MENU_ZAKO32772:
			ZakonczGre(hWnd);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_MOVING:
		{
			changed=(RECT*)lParam;
			GetWindowRect(hWnd,changed);
		}
		break; 
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		if(hWnd != hWndOfMainWindow)
		{
			RECT wrc;
			GetWindowRect(hWnd, &wrc);
			FillRect(hdc, &ps.rcPaint, brushPola);
		}
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		if(hWnd == hWndOfMainWindow)
		{
			PostQuitMessage(0);
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
